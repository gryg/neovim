# Goal

Provide configuration for neovim. If `install.sh` script
is used, vim configuration will also be handled thanks to symlink.

This configuration intend to provide a good python (mainly) workflow development
when using vim/tmux.


# Installation

Install dependency (see below), then run:

```
git clone git@gitlab.com:gryg/neovim.git ~/.config/nvim
cd ~/.config/nvim
./install.sh
```

You then can start neovim and install plugin. Don't forget to check on
[cheatsheet](cheatsheet.md) and plugin documentations for more information.

# Requirement

* git

* make

* gcc

* ctags (for the ctags module)

* python-devel

* libclang (>3.3)

* cmake

* pyflakes (for the pyflakes module)

* ag

You also can add as many linter as you want to use with **neomake**. You
can use linter for other language than python.

* ex : pylint (python)


# Plugin & Doc

Plugins are managed thanks to [VimPlug](https://github.com/junegunn/vim-plug). VimPlug
install is automatic when running `install.sh` script.

## Core Plugin

* [vim-airline](https://github.com/bling/vim-airline.git)

* [neomake](https://github.com/neomake/neomake)

* vim-airline/vim-airline-themes

* mhinz/vim-startify

* tpope/vim-repeat

* vim-scripts/zoomintab

* sjl/gundo

* maxbrunsfeld/vim-yankstack

## Coding

* [YCM](https://github.com/Valloric/YouCompleteMe.git)

* [tagbar](https://github.com/majutsushi/tagbar)

* [delmitmate](https://github.com/Raimondi/delimitMate)

* [nerdcommenter](https://github.com/scrooloose/nerdcommenter)

* [indentation](https://github.com/nathanaelkane/vim-indent-guides.git])

* [vim-better-whitespace](https://github.com/ntpeters/vim-better-whitespace.git)

## Browsing, files

* [nerdtree](https://github.com/vim-scripts/The-NERD-tree.git)

* [BufExplorer](https://github.com/vim-scripts/bufexplorer.zip.git)

* vim-scripts/LargeFile

* travisjeffery/vim-auto-mkdir

* vim-scripts/DirDiff

## Git

* [vim-gitgutter](https://github.com/airblade/vim-gitgutter.git)

*  [fugitive](https://github.com/tpope/vim-fugitive.git)

## Search

* [Multiple Cursor](https://github.com/terryma/vim-multiple-cursors.git)

* [ctrlp]([https://github.com/kien/ctrlp.vim.git)

* [ag](https://github.com/rking/ag.vim)

## Snippets

* ultisnips

* vim-snippets

* [snipmate](https://github.com/msanders/snipmate.vim)

## Themes

* [molokai color scheme](https://github.com/tomasr/molokai.git)

* [solarized](https://github.com/altercation/vim-colors-solarized)

* jnurmine/Zenburn

* **morhetz/gruvbox** (Currently used)

## Language specific

### Rust

* rust-lang/rust

### Javascript

* [javascript](https://github.com/pangloss/vim-javascript)

* [javascript-syntax](https://github.com/jelera/vim-javascript-syntax.git)

* othree/javascript-librairies-syntax

* burnettk/vim-angular

### Python

* [pydoc](https://github.com/fs111/pydoc.vim.git)

* cjrh/vim-conda

* jmcantrell/vim-virtualenv

* hynek/vim-python-pep-indent

### Markdown

* [markdown](https://github.com/plasticboy/vim-markdown.git)

### CSV

* [vim-csv](https://github.com/chrisbra/csv.vim.git)

### css

* [cssimprove](https://github.com/vim-scripts/Better-CSS-Syntax-for-Vim)

* [stylus](https://github.com/wavded/vim-stylus.git)

### html5

* othree/html5

* Rykka/riv

### Ansible & Salt stack

* saltstack/salt-vim

* pearofducks/ansible-vim

### Misc

* pearance/vim-tmux

* vim-scripts/crontab

* Shougo/vinarise

# About the conf

* *mapping.vim* => Contain keyboard mapping

* *filetype.vim* => Contain filetype settings

* *langdep.vim* => Add some language dependecies management

* *plugin.vim* => Plugin settings
