"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" /!\ LEADER KEY /!\
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = ","

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" BUFFERS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Buffer changing / cycling files through buffer
map <S-LEFT> <ESC>:bp<RETURN>
map <S-RIGHT> <ESC>:bn<RETURN>
map <SPACE>k <C-w><up>
map <SPACE>j <C-w><down>
map <SPACE>l <C-w><right>
map <SPACE>h <C-w><left>
" Tab moving
map :ee :tabe
map <SPACE>- :tabp<RETURN>
map <SPACE>+ :tabn<RETURN>
" Resize buffers
map <C-right> <C-W><
map <C-left> <C-W>>
" Yanking stack
nnoremap <leader>p <Plug>yankstack_substitute_older_paste
nnoremap <leader>P <Plug>yankstack_substitute_newer_paste
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PLUGINS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree
map nt :NERDTree `pwd`<RETURN>
" BufExplorer
nmap <silent> <unique> <SPACE>o :BufExplorer<CR>
" Gundo
nnoremap <F5> :GundoToggle<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" EDITING
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" reindent file
map == ggvG=''

" CC/CV
noremap<leader>c <ESC>"*y<RETURN>
noremap<leader>v <ESC>"*p<RETURN>
noremap<leader>C <ESC>"+y<RETURN>
noremap<leader>V <ESC>"+p<RETURN>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MISC
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Esc shortcut
imap jj <Esc>
" disable highlighting : Useful after performing a search
map ** <ESC>:noh<RETURN>
" Folding functions
map -f <ESC>/^}<RETURN><ESC>zf%

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap<leader>l <ESC>:lopen<RETURN>
nnoremap<leader>ll <ESC>:lclose<RETURN>
nnoremap<leader>n <ESC>:lnext<RETURN>
nnoremap<leader>N <ESC>:lprev<RETURN>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" JSON
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map<leader>j :%!python -m json.tool<RETURN>
